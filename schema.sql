DROP DATABASE IF EXISTS wargame;

CREATE DATABASE IF NOT EXISTS wargame;

USE wargame;

CREATE TABLE IF NOT EXISTS games(
  id int not null auto_increment,
  started_at date not null,
  ended_at date,
  state varchar(255) not null,
  primary key (id)
);

CREATE TABLE IF NOT EXISTS players(
  id int not null auto_increment,
  name varchar(24),
  email_encrypted varchar(32) not null,
  password_encrypted varchar(32) not null,
  created_at DATE not null,
  primary key (id)
);

CREATE TABLE IF NOT EXISTS players_games (
  id int not null auto_increment,
  player_id int not null,
  game_id int not null,
  primary key (id),
  foreign key (player_id) references players(id),
  foreign key (game_id) references games(id)
);

CREATE INDEX players_games_games_id_idx
ON players_games (game_id);

CREATE INDEX players_games_players_id_idx
ON players_games (player_id);

CREATE TABLE player_actions (
  id int not null auto_increment,
  player_id int not null,
  game_id int not null,
  timestamp date not null,
  handler varchar(255) not null,
  data text,
  primary key (id),
  foreign key (player_id) references players(id),
  foreign key (game_id) references games(id)
);

CREATE INDEX player_actions_games_id_idx
ON player_actions (game_id);

CREATE INDEX player_actions_players_id_idx
ON player_actions (player_id);

CREATE TABLE state_changes (
  id int not null auto_increment,
  player_action_id int not null,
  timestamp date not null,
  evaluated_at date,
  resolved_at date,
  primary key (id),
  foreign key (player_action_id) references player_actions(id)
);

CREATE INDEX state_changes_player_action_idx
ON state_changes (player_action_id);

